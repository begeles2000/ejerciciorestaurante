package com.restaurante.dao;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.restaurante.common.model.Menu;
import com.restaurante.common.model.Mesa;
import static com.restaurante.common.utils.UtilsFile.crear;
import static com.restaurante.common.utils.UtilsFile.leertxt;
import static com.restaurante.common.utils.UtilsFile.nuevotxt;

public class DaoMesa implements IDao<Mesa> {

	
	
	/**
	 * Constructor por defecto, al arrancar se encarga de crear el json en caso
	 * de que no exista
	 * 
	 * @throws IOException
	 */
	public DaoMesa() throws IOException {
		try {
			crear();
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}

	
	/**
	 * A�ade una nueva mesa al JSON de mesas
	 */
	public void add(Mesa object) throws IOException {
		// Aqui genero una mesa nueva

		// recibo las mesas ya existentes
		JSONObject restaurante = obtenerMesasJson();
		JSONArray list = (JSONArray) restaurante.get("Mesas");

		// genero el nuevo objeto JSON
		JSONObject mesaNueva = new JSONObject();
		mesaNueva.put("id", object.getId());
		mesaNueva.put("nombre", object.getNombre());

		// me creo el array con los pedidos de esa mesa
		JSONArray pedidos = new JSONArray();
		for (Menu pedido : object.getPedidos()) {
			JSONObject ob = new JSONObject();
			ob.put("plato", pedido.toString());
			pedidos.add(ob);
		}
		// a�ado los pedidos
		mesaNueva.put("pedidos", pedidos);

		// lo a�ado al JSON

		list.add(mesaNueva);

		// los escribo
		try (FileWriter file = nuevotxt()) {

			file.write(restaurante.toJSONString());
			file.flush();

		} catch (IOException e) {			
			e.printStackTrace();

			throw e;
		}

	}

	
	/**
	 * Modifica la mesa indicada
	 */
	public void mod(Mesa object) throws IOException {
		// modifico una mesa ya existente
		// borro la antigua
		del(object);
		// meto la nueva
		add(object);

	}

	/**
	 * Elimina una mesa indicada, si no existe no hace nada
	 */
	@Override
	public void del(Mesa object) throws IOException {
		// Elimino una mesa
		// cojo todas las mesas
		JSONObject restaurante = obtenerMesasJson();
		// las meto en un array
		JSONArray list = (JSONArray) restaurante.get("Mesas");

		// borro la que quiero

		for (int i = 0; i < list.size(); i++) {

			JSONObject mesa = (JSONObject) list.get(i);
			if (mesa.get("nombre").equals(object.getNombre())) {
				list.remove(i);
			}
		}

		// guardo el json
		// los escribo
		try (FileWriter file = nuevotxt()) {

			file.write(restaurante.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * Devuelve una lista con todas las mesas existentes en el json
	 */
	@Override
	public List<Mesa> list() throws IOException {
		List<Mesa> listadoFinal = new ArrayList<Mesa>();

		// recojo todas las mesas
		JSONObject restaurante = obtenerMesasJson();
		JSONArray list = (JSONArray) restaurante.get("Mesas");

		for (int i = 0; i < list.size(); i++) {

			Mesa mesatmp = new Mesa();
			JSONObject mesaJson = (JSONObject) list.get(i);
			mesatmp.setId(mesaJson.get("id").toString());
			mesatmp.setNombre(mesaJson.get("nombre").toString());
			List<Menu> pedidos = new ArrayList<Menu>();
			JSONArray pedidostmp = (JSONArray) mesaJson.get("pedidos");

			for (Object ob : pedidostmp) {
				JSONObject obj = (JSONObject) ob;
				Menu obMen = Menu.valueOf((obj.get("plato")).toString());
				pedidos.add(obMen);
			}
			mesatmp.setPedidos(pedidos);

			listadoFinal.add(mesatmp);
		}

		return listadoFinal;
	}

	@Override
	/**
	 * Devuelve la mesa completa, pasandole una mesa que solo contiene el nombre
	 */

	public Mesa search(Mesa mesa) {
		// cojo todas las mesas
		Mesa mesaencontrada = new Mesa();
		JSONObject restaurante = obtenerMesasJson();
		JSONArray mesas = (JSONArray) restaurante.get("Mesas");
		// busco la mesa que tiene el nombre correcto
		for (int i = 0; i < mesas.size(); i++) {
			JSONObject mesatmp = (JSONObject) mesas.get(i);
			// si el nombre es el correcto
			if (mesatmp.get("nombre").equals(mesa.getNombre())) {
				mesaencontrada.setId(mesatmp.get("id").toString());
				mesaencontrada.setNombre(mesatmp.get("nombre").toString());
				// y los pedidos de esa mesa
				List<Menu> pedidos = new ArrayList<Menu>();
				JSONArray pedidostmp = (JSONArray) mesatmp.get("pedidos");

				for (Object ob : pedidostmp) {
					JSONObject obj = (JSONObject) ob;
					Menu obMen = Menu.valueOf((obj.get("plato")).toString());
					pedidos.add(obMen);
				}

				mesaencontrada.setPedidos(pedidos);
				return mesaencontrada;

			}
		}

		return null;
	}

	public JSONObject obtenerMesasJson() {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(leertxt());
		} catch (FileNotFoundException e1) {			
			e1.printStackTrace();
		} catch (IOException e1) {
			
			e1.printStackTrace();
		} catch (ParseException e1) {			
			e1.printStackTrace();
		}

		JSONObject jsonObject = (JSONObject) obj;

		return jsonObject;

	}

}
