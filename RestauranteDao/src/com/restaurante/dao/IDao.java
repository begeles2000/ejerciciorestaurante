package com.restaurante.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.json.simple.parser.ParseException;

import com.restaurante.common.model.Mesa;

public interface IDao<T> {
	
	public void add(T object) throws IOException ;
	public void mod(T object) throws IOException ;
	public void del(T object) throws IOException ;
	public List<T> list() throws IOException ;	
	public T search(Mesa mesa) throws IOException;

}
