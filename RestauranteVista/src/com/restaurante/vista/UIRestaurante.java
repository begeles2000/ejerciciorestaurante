package com.restaurante.vista;

import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.restaurante.common.model.Menu;
import com.restaurante.common.model.Mesa;

public class UIRestaurante {

	/**
	 * Muestra la cadena que recibe por parametro *
	 * 
	 * @param newln
	 *            Si es true hace retorno de carro al fila, si es false no lo
	 *            hace
	 * 
	 */
	public void mostrarCadena(String cadena, boolean newln) {
		if (!newln) {
			System.out.print(cadena);
		} else {
			System.out.println(cadena);
		}
	}

	/**
	 * Solicita una linea por teclado al usuario
	 * 
	 * @return Devuelve la cadena recogida en un string
	 */
	public String pedirLinea() {
		String linea = "";
		boolean valido = false;
		Scanner entrada = new Scanner(System.in);
		do {
			try {
				linea = entrada.nextLine();
				valido = true;
			} catch (InputMismatchException e) {
				e.printStackTrace();
				valido = false;
			}
		} while (!valido);

		return linea;

	}

	/**
	 * Solicita al usuario la respuesta a una pregunta S/N Verifica que es una
	 * opcion correcta
	 * 
	 * @return Devuelve un booleano con la respues del usuario(S->true ,
	 *         N->false)
	 */
	public boolean pedirSN() {
		String opcion = "";
		boolean result = false;
		boolean valido = false;
		Scanner entrada = new Scanner(System.in);
		do {
			try {
				opcion = entrada.nextLine();
				if (opcion.toUpperCase().equals("S") || opcion.toUpperCase().equals("N")) {
					valido = true;
					if (opcion.toUpperCase().equals("S"))
						result = true;
					else
						result = false;

				} else {
					mostrarCadena("Opcion no valida, vuela a probar: ", false);
					valido = false;
				}
			} catch (InputMismatchException e) {
				e.printStackTrace();
				valido = false;
			}
		} while (!valido);

		return result;

	}

	/**
	 * Solicita un numero entero por teclado al usuario
	 * 
	 * @return Devuelve el entero que se ha recogido por teclado
	 */
	public int pedirInt() {
		int numero = 0;
		boolean valido = false;
		Scanner entrada = new Scanner(System.in);
		do {
			try {
				numero = entrada.nextInt();
				valido = true;

			} catch (InputMismatchException e) {

				e.printStackTrace();

			}
		} while (!valido);

		return numero;

	}

	/**
	 * Mete un monton de lineas en blanco para limpiar la consola
	 */
	public void limpiarConsola() {
		for (int i = 0; i < 30; i++) {
			mostrarCadena("", true);
		}
	}

	/**
	 * Muuetra los mensajes que hay que ir sacando a lo largo del proceso de
	 * eliminar una mesa
	 * 
	 * @param paso
	 */
	public void eliminarMesa(int paso) {

		switch (paso) {
		case 1:
			mostrarCadena("Borrando mesa: (esto no se puede deshacer)", true);
			break;
		case 2:
			mostrarCadena("Se va a borrar esta mesa:", true);
			break;
		case 3:
			mostrarCadena("Esta seguro? S/N:", true);
			break;
		case 4:
			mostrarCadena("Mesa Eliminada", true);
			break;
		case 5:
			mostrarCadena("Cancelado", true);
			break;
		default:
			break;
		}

	}

	/**
	 * Solicita al usuario que introduzca un int para las opciones del menu
	 */
	public int elegirOpcion() {
		int opcion = 0;
		boolean valido = false;
		Scanner entrada = new Scanner(System.in);
		do {
			try {
				opcion = entrada.nextInt();
				valido = true;

				if (opcion < 0 || opcion > 4) {
					mostrarCadena("Opcion no existente, siga rascando: ", true);
					valido = false;
				}

			} catch (InputMismatchException e) {
				e.printStackTrace();
				valido = false;
			}
		} while (!valido);

		return opcion;

	}

	/**
	 * Muestro el menu principal de la aplicacion
	 */
	public void menu() {
		mostrarCadena("-----Menu Principal-----", true);
		mostrarCadena("1 - A�adir plato", true);
		mostrarCadena("2 - Obtener Cuenta", true);
		mostrarCadena("3 - Listar Mesas", true);
		mostrarCadena("4 - Borrar Mesa", true);
		mostrarCadena("0 - Salir", true);
		mostrarCadena("Seleccionar opcion: ", false);

	}

	/**
	 * Muestra un info para cuando la mesa no existe
	 */
	public void errorBuscar() {
		mostrarCadena("La mesa seleccionada no existe.", true);
	}

	/**
	 * Lista los platos disponibles en el menu
	 */
	public void listarPlatos() {
		mostrarCadena("Los platos en el menu son:", true);
		int i = 0;
		for (Menu plato : Menu.values()) {

			StringBuilder sb = new StringBuilder();
			sb.append(i);
			sb.append(" - ");
			sb.append(plato.toString());
			String strI = sb.toString();
			mostrarCadena(strI, true);
			i++;
		}

	}

	/**
	 * Solicita una mesa por su nombre a.k.a numero y la devuelve
	 * 
	 * @return
	 */
	public Mesa solicitarMesa() {
		Mesa mesa = new Mesa();
		mostrarCadena("Introduce el nombre de la mesa:", true);
		mesa.setNombre(pedirLinea());
		return mesa;
	}

	/**
	 * Devuelve le plato seleccionado
	 * 
	 * @return
	 */
	public Menu solicitarPlato() {
		mostrarCadena("Selecciona el plato:", true);
		listarPlatos();
		boolean valido = false;
		int platoelegido = -1;
		do {
			platoelegido = pedirInt();
			if (platoelegido < 0 || platoelegido > Menu.values().length) {
				valido = false;
				mostrarCadena("Plato no valido", true);
			} else {
				valido = true;
			}
		} while (!valido);

		Menu elegido = Menu.values()[platoelegido];
		return elegido;
	}

	/**
	 * Mustra el recibo en detalla de una mesa en cuestion
	 * 
	 * @param mesa
	 */
	public void mostrarCuenta(Mesa mesa) {

		DecimalFormat df = new DecimalFormat("#.##");

		StringBuilder sb = new StringBuilder();
		sb.append("Cuenta para la mesa ");
		sb.append(" - ");
		sb.append(mesa.getNombre());
		String strI = sb.toString();
		mostrarCadena(strI, true);

		for (Menu plato : mesa.getPedidos()) {
			sb = new StringBuilder();
			sb.append(plato.toString());
			sb.append("\t");
			sb.append(plato.precio());
			sb.append("�");
			strI = sb.toString();
			mostrarCadena(strI, true);

		}

		sb = new StringBuilder();
		sb.append("Precio\t\t");
		sb.append(df.format(mesa.obtenerTotal()));
		sb.append("�");
		strI = sb.toString();
		mostrarCadena(strI, true);

		sb = new StringBuilder();
		sb.append("Total con IVA\t");
		sb.append(df.format(mesa.obtenerTotal() + mesa.obtenerIva()));
		sb.append("�");
		strI = sb.toString();
		mostrarCadena(strI, true);

	}

	/**
	 * Muesta la informacion basica de una mesa
	 * 
	 * @param mesa
	 */
	public void mostrarMesa(Mesa mesa) {

		mostrarCadena("Nombre Mesa\tTotal", true);

		StringBuilder sb = new StringBuilder();
		sb.append(mesa.getNombre());
		sb.append("\t\t");
		sb.append(mesa.obtenerTotal());
		sb.append("�");
		String strI = sb.toString();
		mostrarCadena(strI, true);

	}

}
