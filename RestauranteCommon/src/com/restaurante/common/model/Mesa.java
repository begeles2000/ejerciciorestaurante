package com.restaurante.common.model;

import java.util.ArrayList;
import java.util.List;

public class Mesa {

	String Id = null;
	String Nombre = null;
	List<Menu> pedidos = new ArrayList<Menu>();

	public Mesa() {
		this.Id = null;
		this.Nombre = null;
		this.pedidos = null;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		this.Id = id;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		this.Nombre = nombre;
	}

	public List<Menu> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Menu> pedidos) {
		this.pedidos = pedidos;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
		result = prime * result + ((Nombre == null) ? 0 : Nombre.hashCode());
		result = prime * result + ((pedidos == null) ? 0 : pedidos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mesa other = (Mesa) obj;
		if (Id == null) {
			if (other.Id != null)
				return false;
		} else if (!Id.equals(other.Id))
			return false;
		if (Nombre == null) {
			if (other.Nombre != null)
				return false;
		} else if (!Nombre.equals(other.Nombre))
			return false;
		if (pedidos == null) {
			if (other.pedidos != null)
				return false;
		} else if (!pedidos.equals(other.pedidos))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String cadena = new StringBuilder().append(this.Id).append(",").append(this.Nombre).toString();

		return cadena;

	}
	/**
	 * Esta maravilla de la ingeneieria devuelve la suma de todo lo que ha pedido la mesa
	 * @return
	 */
	public float obtenerTotal(){
		float total = 0f;
		
		for(Menu pedido: this.pedidos){
			total += pedido.precio();
		}
		
		return total;
	}

	/**
	 * Este metodo devuelve el IVA aplicable a esa mesa
	 * @return
	 */
	public float obtenerIva(){
		float total = 0f;
		
		for(Menu pedido: this.pedidos){
			total += pedido.precio();
		}
		
		float iva = 0f;
		iva = (total/100.f)*21;
		return iva;
	}
}
