package com.restaurante.common.model;

	
	public enum Menu {
		PIZZA(10.2f),		
		MACARRONES (5.2f),
		PAELLA (12f),
		TORTILLA (2.5f),
		CARNE (6.3f);
	
	
	private final float precio;
	
	
	Menu(float precio){
		this.precio = precio;		
	}
	
	public float precio() { return precio; }
	
	}


