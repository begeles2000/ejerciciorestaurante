package com.restaurante.common.utils;

import java.util.UUID;

public class Utils {
	
	/**
	 * Genera un UUID aleatorio y lo devuelve como string.
	 * @return
	 */
	public static String generarId() {
		String uid;
		uid = UUID.randomUUID().toString();
		return uid;
	}
	

}
