package com.restaurante.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class UtilsFile {
	
	private static File fichero;
	
	/**
	 * Comprueba que le fichero de datos JSON existe, si no existe lo crea.
	 * @throws IOException
	 */
 	public static void crear() throws IOException {
 		
 		
 		Properties propiedades = new Properties();
		InputStream entrada = null;
		entrada = new FileInputStream("configuracion.properties");
		propiedades.load(entrada);
		
		fichero = new File(propiedades.getProperty("rutaJsonMesas")); // cargo el nombre de fichero indicado

		if (!fichero.exists()) {
			try {
				//si no esta generado, lo greo y genero el json vacio
				fichero.createNewFile();
				JSONObject resturante = new JSONObject();
				JSONArray list = new JSONArray();
				resturante.put("Mesas",list);
				
				try (FileWriter file = new FileWriter(fichero)) {

					file.write(resturante.toJSONString());
					file.flush();

				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
				
				
			} catch (IOException e) {
				//loggear error: no se ha podido crear el archivo
				e.printStackTrace();
				throw e;
			}
		}
				
	}
	
 	/**
 	 * Devuelve un filereader del fichero de datos
 	 * @return
 	 * @throws IOException
 	 */
	public static FileReader leertxt() throws IOException {
		FileReader fr = null;
		try {
			fr = new FileReader(fichero);
		} catch (IOException ioE) {
			ioE.printStackTrace();
			throw ioE;
		}

		return fr;
	}
	
	/**
	 * Devuelve un filewriter del fichero de datos
	 * @return
	 * @throws IOException
	 */
	public static FileWriter nuevotxt() throws IOException {
		FileWriter fw = null;

		try {
			fw = new FileWriter(fichero);
		} catch (IOException ioE) {
			ioE.printStackTrace();
			throw ioE;
		}

		return fw;
	}
	
	
}
