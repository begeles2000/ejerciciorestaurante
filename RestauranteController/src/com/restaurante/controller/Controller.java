package com.restaurante.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.restaurante.common.model.Menu;
import com.restaurante.common.model.Mesa;
import com.restaurante.common.utils.Utils;
import com.restaurante.dao.DaoMesa;
import com.restaurante.vista.UIRestaurante;

public class Controller {

	UIRestaurante view = new UIRestaurante();
	DaoMesa daomesa = null;
	final static Logger logger = Logger.getLogger(Controller.class);
	/**
	 * Constructor por defecto de controller
	 */
	public Controller() {
		try {
			daomesa = new DaoMesa();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gestiona el menu principal de la aplicacion
	 * 
	 * @throws IOException
	 */
	public void mostrarMenu() throws IOException {
		int op = 0;

		do {
			// muestro el menu
			
			view.menu();
			// solicito la opcion
			op = view.elegirOpcion();

			switch (op) {
			case 1:
				addPlato();
				break;
			case 2:
				obtenerCuenta();
				break;
			case 3:
				listarMesas();
				break;
			case 4:
				borrarMesa();
				break;
			case 0:
				break;
			default:
				break;
			}
		} while (op != 0);

	}

	/**
	 * Se encarga de a�adir un nuevo plato a una mesa
	 */
	public void addPlato() {
		// pedimos la mesa
		Mesa mesa = new Mesa();
		mesa = view.solicitarMesa();
		// pedimos el plato
		
		
		Menu plato = view.solicitarPlato();
		
		// buscamos la mesa, si existe a�adimos el plato
		Mesa mesaEncontrada = daomesa.search(mesa);
		if (mesaEncontrada != null) {
			List<Menu> pedidos = mesaEncontrada.getPedidos();
			pedidos.add(plato);
			mesaEncontrada.setPedidos(pedidos);

			try {
				daomesa.mod(mesaEncontrada);
			} catch (IOException e) {
				logger.error("Error escribir en el fichero para modificar", e);
				e.printStackTrace();
			}

		}
		// si no existe creo la mesa y le a�ado el plato
		else {
			mesaEncontrada = mesa;
			mesaEncontrada.setId(Utils.generarId());
			List<Menu> pedidos = new ArrayList<Menu>();
			pedidos.add(plato);
			mesaEncontrada.setPedidos(pedidos);
			// a�ado la mesa al JSOn

			try {
				daomesa.add(mesaEncontrada);
			} catch (IOException e) {
				logger.error("Error escribir en el fichero para anyadir", e);
				e.printStackTrace();
			}
		}

		
		// nos vamos a casa

	}

	/**
	 * Se encarga de solicitar una mesa y listar la cuenta para esa mesa
	 */
	public void obtenerCuenta() {
		// pido la mesa
		Mesa mesa = new Mesa();
		mesa = view.solicitarMesa();

		// busco la mesa
		Mesa mesaEncontrada = daomesa.search(mesa);
		if (mesaEncontrada != null) {
			view.mostrarCuenta(mesaEncontrada);
		} else {
			view.errorBuscar();
		}

	}

	/**
	 * Lista la informacion basica de todas las mesas actuales
	 */
	public void listarMesas() {
		// recogo todas las mesas
		try {
			List<Mesa> mesas = daomesa.list();
			for (Mesa mesa : mesas) {
				view.mostrarMesa(mesa);
			}

		} catch (IOException e) {
			logger.error("Error leer el fichero para listar las mesas", e);
			e.printStackTrace();
		}

	}

	/**
	 * Se encarga de realizar el proceso de borrar una mesa
	 */
	public void borrarMesa() {
		listarMesas();
		view.eliminarMesa(1);
		Mesa borrar = view.solicitarMesa();
		view.limpiarConsola();
		view.eliminarMesa(2);
		borrar = daomesa.search(borrar);
		view.mostrarMesa(borrar);
		view.eliminarMesa(3);
		boolean conf = view.pedirSN();
		if (conf) {
			try {
				daomesa.del(borrar);
				view.eliminarMesa(4);
			} catch (IOException e) {
				logger.error("Error escribir en el fichero para eliminar", e);
				e.printStackTrace();
			}
		}else{
			view.eliminarMesa(5);
		}

	}
}
